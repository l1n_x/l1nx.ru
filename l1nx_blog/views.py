from django.shortcuts import get_object_or_404, render
from django.views.generic import ListView
from django.core.mail import send_mail
from django.db.models import Count
from django.contrib.auth.models import User
from taggit.models import Tag

from l1nx_blog.models import Post, Comment
from l1nx_blog.forms import EmailPostForm, CommentForm


class PostListView(ListView):
    queryset = Post.objects.filter(status='published')
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'l1nx_blog/list.html'
    tag_slug = None

    def get_queryset(self):
        qs = self.queryset
        if self.tag_slug:
            tag = get_object_or_404(Tag, slug=self.tag_slug)
            qs = qs.filter(tags__in=[tag])
        return qs

    def get(self, request, *args, **kwargs):
        if 'tag' in kwargs:
            self.tag_slug = kwargs['tag']
        return super().get(request, *args, **kwargs)



def post_detail(request, year, month, day, post):
    post = get_object_or_404(Post, slug=post, status='published', date_publish__year=year, date_publish__month=month, date_publish__day=day)
    comments = post.comments.filter(active=True)
    user = User.objects.filter(username=request.user).first()

    post_tags_ids = post.tags.values_list('id', flat=True)
    similar_posts = Post.objects.filter(tags__in=post_tags_ids, status='published').exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags','-date_publish')[:4]

    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.author = user
            new_comment.save()
    else:
        comment_form = CommentForm()

    return render(request,'l1nx_blog/detail.html', {'post': post, 'comments': comments, 'comment_form': comment_form, 'similar_posts': similar_posts})


def post_share(request, post_id):
    post = get_object_or_404(Post, id=post_id, status='published')
    sent = False
    if request.method == 'POST':
        form = EmailPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            post_url = request.build_absolute_uri(post.get_absolute_url())
            subject = '{} ({}) рекомендует к просмотру "{}"'.format(cd['name'], cd['email'], post.title)
            message = 'Читайте "{}" на сайте {}\n\n{} оставил комментарий: {}'.format(post.title, post_url, cd['name'], cd['comments'])
            send_mail(subject, message, 'admin@myblog.com',[cd['to']])
            sent = True
    else:
        form = EmailPostForm()
    return render(request, 'l1nx_blog/share.html', {'post': post, 'form': form, 'sent': sent})