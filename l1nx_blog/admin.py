from django.contrib import admin

from l1nx_blog.models import Post, Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'author', 'date_publish', 'status')
    list_filter = ('status', 'date_created', 'date_publish', 'author')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ('author',)
    date_hierarchy = 'date_publish'
    ordering = ['status', 'date_publish']
    fieldsets = (
        (None, {
            "fields": (
                'title', 'slug', ('author','status'), 'body', 'tags', 'date_publish',
            ),
        }),
    )


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', 'post', 'created', 'active')
    list_filter = ('active', 'created', 'updated')
    search_fields = ('author', 'body')
