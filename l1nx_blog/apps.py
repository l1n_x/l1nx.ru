from django.apps import AppConfig


class L1NxBlogConfig(AppConfig):
    name = 'l1nx_blog'
