from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone
from taggit.managers import TaggableManager


class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Не опубликовано'),
        ('published', 'Опубликовано'),
    )
    title = models.CharField(verbose_name='название', max_length=255, blank=False)
    slug = models.SlugField(verbose_name='путь', max_length=255, blank=False)
    author = models.ForeignKey(User, verbose_name='автор', related_name='blog_posts', on_delete=models.CASCADE)
    body = models.TextField(verbose_name='содержание', blank=False)
    date_publish = models.DateTimeField(verbose_name='дата публикации', default=timezone.now)
    date_created = models.DateTimeField(verbose_name='дата создания', auto_now_add=True)
    date_updated = models.DateTimeField(verbose_name='дата обновления', auto_now=True)
    status = models.CharField(verbose_name='статус', max_length=10, choices=STATUS_CHOICES, default='draft')
    tags = TaggableManager()

    class Meta:
        verbose_name = 'Статья блога'
        verbose_name_plural = 'Статьи блога'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('l1nx_blog:post_detail', args=[self.date_publish.year, self.date_publish.strftime('%m'), self.date_publish.strftime('%d'), self.slug])


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    author = models.ForeignKey(User, verbose_name='Автор', related_name='comments', on_delete=models.CASCADE)
    body = models.TextField(verbose_name='Комментарий')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(verbose_name='Опубликован', default=True)

    class Meta:
        ordering = ('created',)
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return 'Комментарий {} к посту {}'.format(self.author, self.post)