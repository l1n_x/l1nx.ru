from django.urls import path
from .views import monitor_list

app_name='monitor'

urlpatterns = [
    path('', monitor_list, name='main')
]
