import threading
import time
import psutil as ps

RAM = list()
DISK = list()
INTERVAL = 1
ITEMS_COUNT = 100
SM = list()

def timer_thread():
    while True:
        time.sleep(INTERVAL)
        ram = ps.virtual_memory()
        if RAM.__len__() >= ITEMS_COUNT:
            RAM.pop(0)
        if DISK.__len__() >= ITEMS_COUNT:
            DISK.pop(0)
        RAM.append([ram.available / 1024 / 1024])
        DISK.append(ps.disk_usage('/').free / 1024 / 1024)

def start():
    if not SM:
        t = threading.Thread(target=timer_thread,
                            name="Server Monitor",
                            args=(),
                            daemon=True)
        SM.append(t)
        t.start()