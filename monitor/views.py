import datetime
import platform
import threading
import time

import matplotlib as mpl
import mpld3
import psutil as ps
from django.shortcuts import render, redirect
from jinja2 import Markup

mpl.use('agg')

FIGURE_H = 5
FIGURE_W = 10
RAM = list()
DISK = list()
INTERVAL = 1
ITEMS_COUNT = 100
SM = list()


def timer():
    while True:
        time.sleep(INTERVAL)
        ram = ps.virtual_memory()
        if RAM.__len__() >= ITEMS_COUNT:
            RAM.pop(0)
        if DISK.__len__() >= ITEMS_COUNT:
            DISK.pop(0)
        RAM.append([ram.used / 1024 / 1024])
        DISK.append(ps.disk_usage('/').free / 1024 / 1024)

def timer_start():
    if not SM:
        t = threading.Thread(target=timer,
                            name="Server Monitor",
                            args=(),
                            daemon=True)
        SM.append(t)
        t.start()

def get_hdd():
    hdd = ps.disk_usage('/')
    hdd_fig = mpl.pyplot.figure(figsize=(FIGURE_W, FIGURE_H))
    mpl.pyplot.subplot(121)
    labels = ['Свободно', 'Использовано']
    fracs = [hdd.free, hdd.used]
    mpl.pyplot.pie(fracs, labels=labels, autopct='%1.1f%%')
    # mpl.pyplot.subplot(122)
    # mpl.pyplot.plot(DISK)
    # mpl.pyplot.ylabel('МБ')
    # mpl.pyplot.xlabel(f'Интервал {INTERVAL}с')
    # mpl.pyplot.title('График доступности')
    # mpl.pyplot.tight_layout()
    graph = mpld3.fig_to_html(hdd_fig)
    return {
        'title': 'Корневой раздел',
        'graph': Markup(graph),
        'data':{
            'total' : f"Всего памяти: {round(hdd.total / 1024 / 1024 / 1024)} ГБ",
            'list_info' : (
                    f'Свободно памяти: {round(hdd.free / 1024 / 1024)} МБ',
                    f'Использовано памяти: {round(hdd.used / 1024 / 1024)} МБ',
                )
            }
    }

def get_ram():
    ram = ps.virtual_memory()
    ram_fig = mpl.pyplot.figure(figsize=(FIGURE_W, FIGURE_H))
    mpl.pyplot.subplot(121)
    labels = ['Использовано', 'Свободно']
    fracs = [ram.used, ram.free]
    mpl.pyplot.pie(fracs, labels=labels, autopct='%1.1f%%')
    mpl.pyplot.subplot(122)
    mpl.pyplot.plot(RAM)
    mpl.pyplot.ylabel('МБ')
    mpl.pyplot.xlabel(f'Интервал {INTERVAL}с')
    mpl.pyplot.title('График загруженности')
    mpl.pyplot.tight_layout()
    graph = mpld3.fig_to_html(ram_fig)
    return {
        'title': 'Оперативная память',
        'graph': Markup(graph),
        'data':{
            'total' : f"Всего памяти: {round(ram.total / 1024 / 1024 / 1024)} ГБ",
            'list_info' : (
                    f'Доступно памяти: {round(ram.available / 1024 / 1024)} МБ',
                    f'Использовано памяти: {round(ram.used / 1024 / 1024)} МБ',
                )
            }
    }

def monitor_list(request):
    if request.user.is_superuser:
        timer_start()
        active_since = datetime.datetime.fromtimestamp(ps.boot_time())
        context = {
            'system': platform.system(),
            'release': platform.release(),
            'version': platform.version(),
            'active_since': active_since,
            'days_active': (datetime.datetime.now() - active_since).days,
            'CPU': {
                'cores': ps.cpu_count(),
                'load_all': ps.cpu_percent(),
                'load_percpu': ps.cpu_percent(percpu=True),
            },
            'RAM': get_ram(),
            'HDD': get_hdd(),
        }
        return render(request, 'monitor/main.html', context)
    else:
        return redirect('/')
