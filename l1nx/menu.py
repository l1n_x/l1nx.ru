from admin_tools.menu import Menu, items
from django.urls import reverse


class CustomMenu(Menu):
    """
    Custom Menu for l1nx admin site.
    """

    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem('Панель управления', reverse('admin:index')),
            items.Bookmarks(),
            items.AppList(
                'Приложения',
                # children=[
                #     items.ModelList(
                #         title='Главная',
                #         models=('l1nx.*',)
                #     ),
                #     items.ModelList(
                #         title='Статьи блога',
                #         models=('l1nx_blog.*', 'taggit.*', )
                #     ),
                # ],
                exclude=('django.contrib.*',)
            ),
            items.AppList(
                'Администрирование',
                models=('django.contrib.*')
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
