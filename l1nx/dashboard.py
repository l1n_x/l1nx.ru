"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'l1nx.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'l1nx.dashboard.CustomAppIndexDashboard'
"""


from django.urls import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for l1nx.
    """

    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)
        self.children.append(modules.Group(
            title='Приложения',
            display='tabs',
            children=[
                modules.ModelList(
                    title='Главная',
                    models=('l1nx.*',)
                ),
                modules.ModelList(
                    title='Статьи блога',
                    models=('l1nx_blog.*', 'taggit.*')
                ),
            ],
        ))

        self.children.append(modules.AppList(
            'Администрирование',
            models=('django.contrib.*'),
        ))

        self.children.append(modules.RecentActions('Последние изменения', 5))
        # self.children.append(modules.Group(
        #     title=u"Статистика",
        #     display="tabs",
        #     children=[
        #         modules.ModelList(
        #             title = u'Пользователи',
        #             models=(
        #                 'django.contrib.auth.*',
        #                 'my_accounts.models.Profile',
        #             ),
        #         )
        #     ]
        # ))
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Пользователи',
        #         models=(
        #             'django.contrib.auth.*',
        #             'my_accounts.models.Profile',
        #         ),
        #     )
        # )


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for l1nx.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                'Recent Actions',
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
