from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.http import (Http404, HttpResponse, HttpResponseBadRequest,
                         HttpResponsePermanentRedirect, HttpResponseRedirect,
                         JsonResponse)
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.csrf import csrf_exempt, csrf_protect

from l1nx.models import HeaderMenu


def index(request):
    return render(request, 'l1nx/homepage.html')


def logout_view(request):
    logout(request)
    return redirect('/')

def connect_inst(request):
    import requests
    from django.conf import settings
    code = request.GET.get('code')
    state = request.GET.get('state')
    response = requests.post(
        'https://api.instagram.com/oauth/access_token',
        data={
            'client_id': settings.SOCIAL_AUTH_INSTAGRAM_KEY,
            'client_secret': settings.SOCIAL_AUTH_INSTAGRAM_SECRET,
            'grant_type': 'authorization_code',
            'redirect_uri':'https://l1nx.ru/complete/instagram/',
            'code': code,
            'state': state,})
    with open('insta.txt','w') as file:
        file.write(response.text)
    return redirect('/')
