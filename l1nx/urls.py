from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import include, path, re_path
from django.views.generic import RedirectView, TemplateView

from l1nx import views
from l1nx_blog.sitemaps import PostSitemap

sitemaps = {
    'posts': PostSitemap,
}

urlpatterns = [
    re_path(r'^$', views.index, name="home"),
    re_path(r'^admin_tools/', include('admin_tools.urls')),
    path('admin/', admin.site.urls),
    path('blog/', include('l1nx_blog.urls', namespace='l1nx_blog'), name='l1nx_blog'),
    path('monitor/', include('monitor.urls', namespace='monitor'), name='monitor'),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
]
urlpatterns += [
    path('logout/', views.logout_view, name='logout'),
    re_path(r'^complete/instagram/$', views.connect_inst, name='instagram-complete'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
