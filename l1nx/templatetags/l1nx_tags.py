import re
from itertools import chain

from django import template

from l1nx.models import HeaderMenu

register = template.Library()


@register.filter
def to_class_name(value):
    return value.__class__.__name__


@register.simple_tag
def get_canonical_url(request):
    return 'https://' + request.META.get('HTTP_HOST') \
            + request.META.get('PATH_INFO')

@register.simple_tag
def get_header_menu():
    return HeaderMenu.objects.all()
