from django.contrib import admin

from l1nx.models import HeaderMenu


@admin.register(HeaderMenu)
class HeaderMenuAdmin(admin.ModelAdmin):
    list_display = ('title', 'link')