# Generated by Django 3.0.3 on 2020-02-28 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HeaderMenu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('link', models.URLField(verbose_name='Ссылка')),
                ('icon', models.ImageField(blank=True, upload_to='', verbose_name='Иконка')),
                ('weight', models.PositiveSmallIntegerField(verbose_name='Вес')),
            ],
            options={
                'verbose_name': 'Пункт меню',
                'verbose_name_plural': 'Пункты меню',
                'ordering': ('weight',),
            },
        ),
    ]
