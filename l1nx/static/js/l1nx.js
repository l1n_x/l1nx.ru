$( document ).ready(function() {
    var csrftoken = getCookie('csrftoken');
    $.ajaxSetup({
        beforeSend: function(xhr) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    });
})