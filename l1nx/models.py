from django.db import models


class HeaderMenu(models.Model):
    title = models.CharField(verbose_name='Название', max_length=50, blank=False)
    link = models.CharField(verbose_name='Ссылка', max_length=255, blank=False)
    icon = models.ImageField(verbose_name='Иконка', blank=True)
    weight = models.PositiveSmallIntegerField(verbose_name='Вес', blank=False)

    class Meta:
        ordering = ('weight',)
        verbose_name = 'Пункт меню'
        verbose_name_plural = 'Пункты меню'

    def __str__(self):
        return self.title